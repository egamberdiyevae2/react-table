import React from "react";
import ReactDOM from "react-dom/client";

class Counter extends React.Component {
  render() {
    return (
      <div className="allElement">
        <div className="start">
          <h1 className="header"> &lt;header&gt; </h1>
          <h1 className="nav"> &lt;nav&gt; </h1>
        </div>
        <div className="main">
          <div className="section">
            <h1 className="section-item">&lt;section&gt;</h1>
            <h1 className="article">&lt;article&gt;</h1>
          </div>
          <h1 className="aside">&lt;aside&gt;</h1>
        </div>
        <h1 className="footer">&lt;footer&gt;</h1>
      </div>
    );
  }
}

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Counter />
      </div>
    );
  }
}

ReactDOM.createRoot(document.getElementById("root")).render(<App />);
